## sys_mssi_64_cn_armv82-user 14 UKQ1.230924.001 1700816782397 release-keys
- Manufacturer: oplus
- Platform: mt6893
- Codename: ossi
- Brand: oplus
- Flavor: sys_mssi_64_cn_armv82-user
- Release Version: 14
- Kernel Version: 4.19.191
- Id: UKQ1.230924.001
- Incremental: 1700816782397
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: oplus/ossi/ossi:12/SP1A.210812.016/1700753907022:user/release-keys
- OTA version: 
- Branch: sys_mssi_64_cn_armv82-user-14-UKQ1.230924.001-1700816782397-release-keys
- Repo: oplus_ossi_dump_24534
